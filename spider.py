from urllib.request import urlopen
from link_finder import LinkFinder
from domain import *
from general import *
from bs4 import BeautifulSoup
import requests
from details import get_estate_detail


class Spider:

    project_name = ''
    base_url = ''
    domain_name = ''
    queue_file = ''
    crawled_file = ''
    detail_file = ''
    first_url = ''
    queue = set()
    crawled = set()

    def __init__(self, project_name, base_url, domain_name, first_website):
        Spider.project_name = project_name
        Spider.base_url = base_url
        Spider.domain_name = domain_name
        #get the first url not the base
        Spider.first_url = first_website
        Spider.queue_file = Spider.project_name + '/queue.txt'
        Spider.crawled_file = Spider.project_name + '/crawled.txt'
        Spider.detail_file = Spider.project_name + '/details.txt'
        self.boot()
        self.crawl_page('First spider', Spider.first_url)

    # Creates directory and files for project on first run and starts the spider
    @staticmethod
    def boot():
        create_project_dir(Spider.project_name)
        create_data_files(Spider.project_name, Spider.first_url)
        Spider.queue = file_to_set(Spider.queue_file)
        Spider.crawled = file_to_set(Spider.crawled_file)

    # Updates user display, fills queue and updates files
    @staticmethod
    def crawl_page(thread_name, page_url):
        if page_url not in Spider.crawled:
            print(thread_name + ' now crawling ' + page_url)
            print('Queue ' + str(len(Spider.queue)) + ' | Crawled  ' + str(len(Spider.crawled)))
            Spider.add_links_to_queue(Spider.gather_links(page_url))
            Spider.queue.remove(page_url)
            Spider.crawled.add(page_url)
            #get detail
            Spider.get_detail(page_url)
            Spider.update_files()

    #Collect links here
    @staticmethod
    def gather_links(page_url):
        link_set = set()

        #get request from website
        r = requests.get(page_url)
        soup = BeautifulSoup(r.content)

        #block data for getting page links
        block_data1 = soup.find_all('ul', {'class': 'linkList horizontalLinkList pagination'})

        #not get the previous and next link
        if len(block_data1) != 0:
            """page_links = block_data1[0].find_all('a', attrs={'title': lambda x: x and 'previous' not
                              in x and 'next' not in x})"""
            page_links = block_data1[0].find_all('a', attrs={'title': lambda x: x and 'next' in x})

            #add page links to set
            for link in page_links:
                new_link = Spider.base_url + link.attrs['href']
                link_set.add(new_link)

        #block data for getting detail links
        block_data2 = soup.find_all('article', attrs={'class': lambda x: x and 'resultBody' in x})

        if len(block_data2) != 0:
            #add detail links to set
            for item in block_data2:
                new_link = item.find_all('a', attrs={'href': True})
                link_set.add(Spider.base_url + new_link[0].attrs['href'])

        return link_set


    # Saves queue data to project files
    @staticmethod
    def add_links_to_queue(links):
        for url in links:
            if (url in Spider.queue) or (url in Spider.crawled):
                continue
            if Spider.domain_name != get_domain_name(url):
                continue
            Spider.queue.add(url)

    @staticmethod
    def update_files():
        set_to_file(Spider.queue, Spider.queue_file)
        set_to_file(Spider.crawled, Spider.crawled_file)

    @staticmethod
    #get detail in each detail link
    def get_detail(url):
        #check for the detail link
        if 'property' in url:
            details = get_estate_detail(url)
            write_details_to_file(details, Spider.detail_file)

