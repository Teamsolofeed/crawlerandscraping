import requests
from bs4 import BeautifulSoup

def get_estate_detail(url):
    #init dictionary
    all_detail = {'agent_details': [], 'address_detail': {}, 'property_no': '', 'agency_brand_details': {},
                  'description_detail': {}, 'feature_detail': {}, 'price_detail': ''}

    #get raw html
    r = requests.get(url)

    #init beautiful soup
    soup = BeautifulSoup(r.content, from_encoding="utf-8")
    
    #find specific block
    block_data = soup.find_all("div", {"id": "detailsCont"})
    for block_datum in block_data:
        #get property number
        try:
           property_no = block_datum.contents[0].find_all("span", {"class": "property_id"})[0].contents[0]
           all_detail['property_no'] = property_no
        except:
            print('Cannot fine property_no')
            pass

        #get price
        try:
            price = block_datum.contents[0].find_all('p', {'class': 'priceText'})
            all_detail['price_detail'] = price[0].text
        except:
            print('Cannot find price_detail')
            pass

        #get street address, locality, address region, postal code
        try:
            mainAddress = block_datum.contents[0].find_all('span', {'itemprop': 'streetAddress'})
            all_detail['address_detail']['main_address'] = mainAddress[0].text
        except:
            print('Cannot find main address')
            pass

        try:
            addressLocality = block_datum.contents[0].find_all('span', {'itemprop': 'addressLocality'})
            all_detail['address_detail']['address_locality'] = addressLocality[0].text
        except:
            print('Cannot find address locality')
            pass

        try:
            addressRegion = block_datum.contents[0].find_all('span', {'itemprop': 'addressRegion'})
            all_detail['address_detail']['address_region'] = addressRegion[0].text
        except:
            print('Cannot find address region')
            pass

        try:
            postalCode = block_datum.contents[0].find_all('span', {'itemprop': 'postalCode'})
            all_detail['address_detail']['postal_code'] = postalCode[0].text
        except:
            print('Cannot find postal code')
            pass

        #agent detail
        try:
            agentInfoExpanded = block_datum.contents[0].find_all('div', {'id': 'agentInfoExpanded'})
            agentDetails = agentInfoExpanded[0].find_all('div', {'class': 'agent'})
            for agentDetail in agentDetails:
                #agent name
                agent_detail = {}
                agentName = agentDetail.contents[2].find_all('p', {'class': 'agentName'})
                agent_detail['agent_name'] = agentName[0].text
                #agent phone
                agentPhone = agentDetail.contents[2].find_all('li', {'class': 'phone'})
                agent_detail['agent_phone'] = agentPhone[0].text
                #append to the list
                all_detail['agent_details'].append(agent_detail)
        except:
            print('Cannot find agent info expanded')
            pass

        #agency brand
        try:
            agencyName = agentInfoExpanded[0].find_all('p', {'class': 'agencyName'})
            all_detail['agency_brand_details']['agency_name'] = agencyName[0].text
            agencyAddress = agentInfoExpanded[0].find_all('div', {'class': 'adr'})
            all_detail['agency_brand_details']['agency_address'] = agencyAddress[0].text
        except:
            print("Cannot find agency brand")

        #description title, body
        try:
            description = block_datum.contents[0].find_all('div', {'id': 'description'})
            descriptionTitle = description[0].find_all('p', {'class': 'title'})
            all_detail['description_detail']['description_title'] = descriptionTitle[0].text
            descriptionBody = description[0].find_all('p', {'class': 'body'})
            moreDescriptionBodyDetail = descriptionBody[0].find_all('span')
            if len(moreDescriptionBodyDetail) != 0:
                fullDescriptionDetail = descriptionBody[0].text + moreDescriptionBodyDetail[0].attrs['data-description']
                fullDescriptionDetail = fullDescriptionDetail.replace('...show more', '')
            else:
                fullDescriptionDetail = descriptionBody[0].text
            all_detail['description_detail']['description_body'] = fullDescriptionDetail
        except:
            print('Cannot find description')

        #features
        try:
            generalFeature = block_datum.contents[0].find_all('div', {'id': 'features'})
            all_detail['feature_detail'] = generalFeature[0].text
        except:
            print('Cannot find feature detail')
            pass

    return all_detail