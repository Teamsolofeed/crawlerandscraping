![](http://i.imgur.com/wYi2CkD.png)


# Overview from author

This is an open source, multi-threaded website crawler written in Python. There is still a lot of work to do, so feel free to help out with development.

***

Note: This is part of an open source search engine. The purpose of this tool is to gather links **only**. The analytics, data harvesting, and search algorithms are being created as separate programs.

***
# Overview from me
Adding data scraping function to the current author's implementation.

### How to use:
 - Just run: python main.py in the cmd prompt and the result will be put in realestate file

 

### Links

- [Support thenewboston](https://www.patreon.com/thenewboston)
- [thenewboston.com](https://thenewboston.com/)
- [Facebook](https://www.facebook.com/TheNewBoston-464114846956315/)
- [Twitter](https://twitter.com/bucky_roberts)
- [Google+](https://plus.google.com/+BuckyRoberts)
- [reddit](https://www.reddit.com/r/thenewboston/)