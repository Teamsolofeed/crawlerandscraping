import os


# Each website is a separate project (folder)
def create_project_dir(directory):
    if not os.path.exists(directory):
        print('Creating directory ' + directory)
        os.makedirs(directory)


# Create queue and crawled files (if not created)
def create_data_files(project_name, base_url):
    queue = os.path.join(project_name , 'queue.txt')
    crawled = os.path.join(project_name,"crawled.txt")
    details = os.path.join(project_name, "details.txt")
    if not os.path.isfile(queue):
        write_file(queue, base_url)
    if not os.path.isfile(crawled):
        write_file(crawled, '')
    if not os.path.isfile(details):
        write_file(details, '')


# Create a new file
def write_file(path, data):
    with open(path, 'w') as f:
        f.write(data)


# Add data onto an existing file
def append_to_file(path, data):
    with open(path, 'a') as file:
        file.write(data + '\n')


# Delete the contents of a file
def delete_file_contents(path):
    open(path, 'w').close()


# Read a file and convert each line to set items
def file_to_set(file_name):
    results = set()
    with open(file_name, 'rt') as f:
        for line in f:
            results.add(line.replace('\n', ''))
    return results


# Iterate through a set, each item will be a line in a file
def set_to_file(links, file_name):
    with open(file_name,"w") as f:
        for l in sorted(links):
            f.write(l+"\n")

def write_details_to_file(details, file_name):
    with open(file_name, "ab") as f:
        try:
            #property no
            f.write(b'property_no:' + details['property_no'].encode('utf-8'))
            f.write(b'\n')

            #address
            f.write(b'address_detail: ' + details['address_detail']['main_address'].encode('utf-8') + b' '
                                             + details['address_detail']['address_locality'].encode('utf-8')
                                             + b' ' + details['address_detail']['address_region'].encode('utf-8')
                                             + b' ' + details['address_detail']['postal_code'].encode('utf-8'))
            f.write(b'\n')

            #agent
            for item in details['agent_details']:
                f.write(b'agent_name: ' + item['agent_name'].encode('utf-8') + b' ')
                f.write(b'agent_phone: ' + item['agent_phone'].encode('utf-8') + b' ')
                f.write(b'\n')

            #agent feature
            f.write(b'agent feature: ' + details['feature_detail'].encode('utf-8'))
            f.write(b'\n')

            #agency brand
            f.write(b'agency name: ' + details['agency_brand_details']['agency_name'].encode('utf-8'))
            f.write(b'agency address: ' + details['agency_brand_details']['agency_address'].encode('utf-8'))
            f.write(b'\n')

            #price
            if len(details['price_detail']) != 0:
                f.write(b'price_detail: ' + details['price_detail'].encode('utf-8'))
            else:
                f.write(b'price_detail: ' + b'Contact Agent')
            f.write(b'\n')

            #description
            f.write(b'description title: ' + details['description_detail']['description_title'].encode('utf-8'))
            f.write(b'\n')
            f.write(b'description: ' + details['description_detail']['description_body'].encode('utf-8'))
            f.write(b'\n\n')
        except:
            f.write(b'Cannot encode')
            f.write(b'\n\n')
            print("Cannot encode")
